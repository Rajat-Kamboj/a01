// import dependencies
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const fileupload = require('express-fileupload');
const session = require('express-session');
const { check, validationResult } = require('express-validator');
const fileUpload = require('express-fileupload');

// set up DB connection and connect to DB
mongoose.connect('mongodb://localhost:27017/travelBlog', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// set up the model for admin
const Admin = mongoose.model('Admin', {
  username: String,
  password: String,
});

// set up the model for page
const Page = mongoose.model('Page', {
  title: String,
  author: String,
  content: String,
  imageName: String,
  lastModified: Date,
});

// set up variable to use packages
var myApp = express();
myApp.use(express.urlencoded({ extended: false }));

// set up session
myApp.use(
  session({
    secret: 'ain749jp7vbQAaP7rKeoJ3dm7vBPBhwh',
    resave: false,
    saveUninitialized: true,
  })
);

// set path to view folders
myApp.set('views', path.join(__dirname, 'views'));

// use public folder
myApp.use(express.static(__dirname + '/public'));

// set view engine
myApp.set('view engine', 'ejs');

// use file upload
myApp.use(fileUpload());

// set up different routes of the website

// login page GET
myApp.get('/login', function (req, res) {
  Page.find({}).exec(function (err, links) {
    res.render('login', { links: links });
  });
});

// login page POST
myApp.post('/login', function (req, res) {
  var username = req.body.username;
  var password = req.body.password;

  Admin.findOne({ username: username, password: password }).exec(function (
    err,
    admin
  ) {
    if (admin) {
      // store username in session and set logged in to true
      req.session.username = username;
      req.session.userLoggedIn = true;
      console.log('Login successful. Admin: ' + username);
      res.redirect('/allpages');
    } else {
      console.log('Login failed for Admin: ' + username);
      console.log('Error while login: ' + err);
      Page.find({}).exec(function (err, links) {
        res.render('result', { message: 'Sorry, cannot login!', links: links });
      });
    }
  });
});

// home page GET
myApp.get('/', function (req, res) {
  Page.findOne({ title: 'Home' }).exec(function (err, page) {
    if (err) {
      console.log('Error: ' + error);
      res.render(result, { message: 'Kinldy add a page with title as Home ' });
    } else if (page) {
      Page.find({}).exec(function (error, links) {
        res.redirect('/view/' + page._id);
      });
    }
  });
});

// preview page GET with ID
myApp.get('/view/:id', function (req, res) {
  var id = req.params.id;
  Page.findOne({ _id: id }).exec(function (err, page) {
    if (page) {
      Page.find({}).exec(function (error, links) {
        res.render('home', { page: page, links: links });
      });
    }
  });
});

// All pages GET
myApp.get('/allpages', function (req, res) {
  if (req.session.userLoggedIn) {
    Page.find({}).exec(function (err, pages) {
      if (err) {
        console.log('Error: ' + err);
      }
      res.render('allpages', { pages: pages });
    });
  } else {
    Page.find({}).exec(function (err, links) {
      res.redirect('/login', { links: links });
    });
  }
});

// Add page GET
myApp.get('/addpage', function (req, res) {
  if (req.session.userLoggedIn) {
    res.render('addpage');
  } else {
    Page.find({}).exec(function (err, links) {
      res.render('login', { links: links });
    });
  }
});

// Add page POST
myApp.post(
  '/addpage',
  [
    check('title', 'Title cannot be empty').not().isEmpty(),
    check('content', 'Content cannot be empty').not().isEmpty(),
    check('sportShoeQuantity').custom((value, { req }) => {
      var cost1 = value * sportShoeCost;
      var cost2 = req.body.formalShoeQuantity * formalShoeCost;
      if (cost1 + cost2 < 10) {
        throw new Error('Minimum total amount should be $10');
      }
      return true;
    }),
  ],
  function (req, res) {
    const validationErrors = validationResult(req);
    console.log(validationErrors);
    if (!validationErrors.isEmpty()) {
      res.render('addpage', { errors: validationErrors.array() });
    } else if (req.session.userLoggedIn) {
      //fetch all form fields
      var title = req.body.title;
      var content = req.body.content;
      var imageName = req.files.image.name;
      var imageFile = req.files.image;
      var imagePath = 'public/view/uploads/' + imageName;

      // move temporary image file to uploads folder
      imageFile.mv(imagePath, function (err) {
        if (err) {
          console.log('Error for image move: ' + err);
        }
      });

      // fetch author from session
      var author = req.session.username;
      var lastModified = new Date();

      var newPage = new Page({
        title: title,
        content: content,
        imageName: imageName,
        author: author,
        lastModified: lastModified,
      });
      newPage.save();
      Page.find({}).exec(function (err, links) {
        res.render('result', {
          message: 'New Page added successfully',
          links: links,
        });
      });
    } else {
      res.render('/login');
    }
  }
);

// Edit page GET
myApp.get('/edit/:id', function (req, res) {
  if (req.session.userLoggedIn) {
    var id = req.params.id;
    Page.findOne({ _id: id }).exec(function (err, page) {
      if (page) {
        res.render('editpage', { page: page });
      } else {
        console.log('Error: ' + err);
        res.render('result', { message: 'Page not found' });
      }
    });
  } else {
    Page.find({}).exec(function (err, links) {
      res.render('login', { links: links });
    });
  }
});

// Edit page POST
myApp.post(
  '/edit/:id',
  [
    check('title', 'Title cannot be empty').not().isEmpty(),
    check('content', 'Content cannot be empty').not().isEmpty(),
  ],
  function (req, res) {
    var id = req.params.id;
    var validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
      Page.findOne({ _id: id }).exec(function (er, page) {
        res.render('editpage', {
          errors: validationErrors.array(),
          page: page,
        });
      });
    } else if (req.session.userLoggedIn) {
      //fetch all form fields
      var title = req.body.title;
      var content = req.body.content;
      var imageName = req.files.image.name;
      var imageFile = req.files.image;
      var imagePath = 'public/view/uploads/' + imageName;

      // move temporary image file to uploads folder
      imageFile.mv(imagePath, function (err) {
        if (err) {
          console.log('Error for image move: ' + err);
        }
      });

      // fetch author from session
      var author = req.session.username;
      var lastModified = new Date();

      Page.findOne({ _id: id }).exec(function (err, page) {
        Page.find({}).exec(function (err, links) {
          if (page) {
            page.title = title;
            page.content = content;
            page.imageName = imageName;
            page.author = author;
            page.lastModified = lastModified;
            page.save();
            res.render('result', {
              message: 'Page edited succesfully',
              links: links,
            });
          } else {
            res.render('result', { message: 'Page not found', links: links });
          }
        });
      });
    } else {
      res.render('login');
    }
  }
);

// Delete page GET
myApp.get('/delete/:id', function (req, res) {
  if (req.session.userLoggedIn) {
    var id = req.params.id;
    Page.findByIdAndDelete({ _id: id }).exec(function (err, page) {
      if (err) {
        console.log('Error: ' + error);
      }
      Page.find({}).exec(function (err, links) {
        if (page) {
          res.render('result', { message: 'Page deleted', links: links });
        } else {
          res.render('result', {
            message: 'Error Occured. Page could not be deleted',
            links: links,
          });
        }
      });
    });
  } else {
    res.render('login');
  }
});

// Logout
myApp.get('/logout', function (req, res) {
  req.session.username = '';
  req.session.userLoggedIn = false;
  Page.find({}).exec(function (err, links) {
    res.render('result', { message: 'Successfully logged out', links: links });
  });
});

// start the server and listen at a port
myApp.listen(8080);

// log successful execution
console.log('LISTENING AT PORT 8080');
